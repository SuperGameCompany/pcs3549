﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : MonoBehaviour {

  public GameObject enemyBulletPrefab;
  public static Vector2 shootDirection;
  public float shootInterval = 1f; //Time between each shoot
  public float shootSpeed = 1.2f;

  public bool isShooting = false;
  protected GameObject player;
	// Use this for initialization
	void Start () {
    player = GameObject.Find("Player");
    // StartCoroutine(ShootLoop());
	}
	
	// Update is called once per frame
	void Update () {
	}

  public virtual IEnumerator ShootLoop() {
    EnemyMovement em = GetComponent<EnemyMovement>();
    if (em.isInAttackingPosition && !isShooting) {
      print("Atirando..");
      Vector3 selfPosition = transform.position;
      Vector3 playerPosition = player.transform.position;
      shootDirection = playerPosition - selfPosition;
      shootDirection.Normalize();
      //this gives the angle between the x axis and the shooting vector
      float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;
      Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
      GameObject newBullet = Instantiate(enemyBulletPrefab, transform.position + (Vector3)(shootDirection * 0.2f), rotation);
      newBullet.GetComponent<Rigidbody2D>().velocity = shootDirection * shootSpeed;
      isShooting = true;
      yield return new WaitForSeconds(shootInterval);
      isShooting = false;
    } 
  }
}
