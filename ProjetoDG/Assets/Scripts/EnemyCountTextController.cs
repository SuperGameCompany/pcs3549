﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyCountTextController : MonoBehaviour {
    public TextMeshProUGUI enemiesSlain;
    public TextMeshProUGUI maxEnemies;

    // Use this for initialization
    void Start() {
        enemiesSlain = GameObject.Find("EnemiesSlain").GetComponent<TextMeshProUGUI>();
        maxEnemies = GameObject.Find("MaxEnemies").GetComponent<TextMeshProUGUI>();
        maxEnemies.SetText(GameManager.Instance.maxEnemies.ToString());
        enemiesSlain.SetText("0");
    }

    public void UpdateEnemiesSlainText(string newValue) {
        enemiesSlain.SetText(newValue);

    }

    public void IncrementDeadEnemies() {
        GameManager.Instance.deadEnemies++;
        UpdateEnemiesSlainText(GameManager.Instance.deadEnemies.ToString());
    }
}
