﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour {
    public GameObject pauseMenu;

	// Update is called once per frame
	void Update () {
        ProcessInput();
    }

    public void Resume() {
        GameManager.Instance.gameIsPaused = false;
        GameManager.Instance.ableToInput = true;
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Pause() {
        GameManager.Instance.gameIsPaused = true;
        GameManager.Instance.ableToInput = false;
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }

    public void MenuButton() {
        SceneManager.LoadScene("MainMenu");
    }

    public void ProcessInput() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (GameManager.Instance.gameIsPaused) {
                Resume();
            }
            else {
                Pause();
            }
        }
    }
}
