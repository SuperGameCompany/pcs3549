using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public GameObject bullet;
    public GameObject arms;
    public BoxCollider2D boxCollider;
    public float walkSpeed = 2f;
    public GameOverManager gameOverManager;
    public static Vector2 shootDirection;
    public float immunityTime = 2f;
    public int totalLives = 1;
    public bool alreadyDied = false;
    public FlashAnimationManager flashAnimationManager;
    public PlayerBulletScript playerBulletScriptDefault;
    public PlayerBulletMetal playerBulletScriptMetal;
    public PlayerBulletFire playerBulletScriptFire;
    public PlayerBulletWater playerBulletScriptWater;
    public PlayerBulletThunder playerBulletScriptThunder;
    public Animator playerAnimator;
    public Animator armsAnimator;
    public IList<Object> lifes = new List<Object>();
    public Object weaponSelectedOnHud; // XGH ultra generic type
    public bool oneLifeTaken = false;
    public bool canBeHit = true;
    AnimatorStateInfo animatorState;
    public bool hasShotRecently = false;
    Hashtable prefabs = new Hashtable();
    public List<string> weaponsAvailable;
    public Hashtable weaponIcons = new Hashtable();
    public Hashtable selectedWeaponIcons = new Hashtable();
    public IList<Object> screenWeapons = new List<Object>();

    private string selectedShot = "default";
    private SpriteRenderer[] sprites;
    Hashtable armsSprites = new Hashtable();

    void Start()
    {
        GameManager.Instance.ResetInitialVariables();

        flashAnimationManager = GameObject.Find("FlashAnimationManager").GetComponent <FlashAnimationManager>();
        gameOverManager = GameObject.Find("GameOverManager").GetComponent<GameOverManager>();
        boxCollider = GetComponent<BoxCollider2D>();
        // Desenha as vidas do jogador
        for (int i = 0; i < totalLives; i++)
        {
            Object life = (GameObject) Resources.Load("Prefabs/Life");
            lifes.Add(Instantiate(life, new Vector3(-2.62f + i * 0.2f, -1.775f, 0), Quaternion.identity));
        }
        sprites = GetComponentsInChildren<SpriteRenderer>();

        weaponsAvailable = new List<string>();
        InitializeAvailableWeapons();

        //populate the bullet prefabs list and shooting scripts
        InitializeBullets();

        DrawWeapons();
        //carrega os bracos possiveis do jogador
        LoadArms();
        
    }

    void Update()
    {
        
        if (Input.GetKey(KeyCode.Alpha1))
        {
            selectedShot = "default";
            bullet = (GameObject)prefabs["default"];
            DrawWeapons();
            GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites["defaultgun"];
        }
        else if (GameManager.Instance.unlockedMetalBullet && Input.GetKey(KeyCode.Alpha2))
        {
            selectedShot = "metal";
            bullet = (GameObject)prefabs["metal"];
            DrawWeapons();
            GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites["metalgun"];
        }
        else if (GameManager.Instance.unlockedThunderBullet && Input.GetKey(KeyCode.Alpha3))
        {
            selectedShot = "thunder";
            bullet = (GameObject)prefabs["thunder"];
            DrawWeapons();
            GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites["thundergun"];
        }
        else if (GameManager.Instance.unlockedWaterBullet && Input.GetKey(KeyCode.Alpha4))
        {
            selectedShot = "water";
            bullet = (GameObject)prefabs["water"];
            DrawWeapons();
            GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites["watergun"];
        }
        else if (GameManager.Instance.unlockedFireBullet && Input.GetKey(KeyCode.Alpha5))
        {
            selectedShot = "fire";
            bullet = (GameObject)prefabs["fire"];
            DrawWeapons();
            GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites["firegun"];
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
          selectedShot = weaponsAvailable[(weaponsAvailable.FindIndex(s => s == selectedShot) + 1) % weaponsAvailable.Count];
          bullet = (GameObject)prefabs[selectedShot];    
          DrawWeapons();
          GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites[selectedShot + "gun"];
        }
        else if (Input.GetKeyUp(KeyCode.Q))
        {
          selectedShot = weaponsAvailable[(weaponsAvailable.FindIndex(s => s == selectedShot) - 1 + weaponsAvailable.Count) % weaponsAvailable.Count];
          bullet = (GameObject)prefabs[selectedShot];    
          DrawWeapons();
          GameObject.Find("Arms/Gun").GetComponent<SpriteRenderer>().sprite = (Sprite)armsSprites[selectedShot + "gun"];
        }
        Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (!alreadyDied)
        {
            if (worldMousePosition.x > transform.position.x)
            {
                foreach (SpriteRenderer sprite in sprites)
                {
                    sprite.flipX = true;
                    arms.transform.right = ((Vector2)worldMousePosition - (Vector2)transform.position).normalized;
                }
            }
            else if (worldMousePosition.x < transform.position.x)
            {
                foreach (SpriteRenderer sprite in sprites)
                {
                    sprite.flipX = false;
                    arms.transform.right = -((Vector2)worldMousePosition - (Vector2)transform.position).normalized;
                }
            }
        }

        if (GameManager.Instance.ableToInput && Input.GetKey(KeyCode.Mouse0))
        {
            StartCoroutine(Shoot());
        }
        CheckIfIsBeingHit();
        CheckIfDied();
    }

    void FixedUpdate()
    {
        ProcessWalkInput();
    }

    public void ProcessWalkInput()
    {
        if (!GameManager.Instance.ableToInput)
        {
            return;
        }

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetAxisRaw("Vertical");

        if (x == 0 && y == 0)
        {
            playerAnimator.SetBool("IsWalking", false);
        }
        else
        {
            playerAnimator.SetBool("IsWalking", true);
        }

        transform.Translate(x * Time.fixedDeltaTime * walkSpeed, y * Time.fixedDeltaTime * walkSpeed, 0);
    }

    IEnumerator Shoot()
    {
        float intervalSpeed = 0.1f;
        if (!hasShotRecently)
        {
            hasShotRecently = true;
            Vector3 selfPosition = transform.position;
            Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            intervalSpeed = ProcessShoot(selectedShot, bullet, selfPosition, worldMousePosition);
            yield return new WaitForSeconds(intervalSpeed);
            PlayShotSound(selectedShot);
            hasShotRecently = false;
        }
    }

    public void HasBeenHit()
    {
        canBeHit = false;
        playerAnimator.SetBool("IsBeingHit", true);
        armsAnimator.SetBool("IsBeingHit", true);
    }

    IEnumerator BlinkingAnimationTimer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        playerAnimator.SetBool("IsBeingHit", false);
        armsAnimator.SetBool("IsBeingHit", false);
    }

    public void CheckIfIsBeingHit()
    {
        animatorState = playerAnimator.GetCurrentAnimatorStateInfo(1);

        if (animatorState.IsName("PlayerBlinkingAnimation"))
        {
            if (!oneLifeTaken && lifes.Count > 0)
            {
                flashAnimationManager.FlashHitAnimation();
                Object life = lifes[lifes.Count - 1];
                Destroy(life);
                lifes.RemoveAt(lifes.Count - 1);
                oneLifeTaken = true;
                StartCoroutine(BlinkingAnimationTimer(immunityTime));
            }
        }
        else
        {
            canBeHit = true;
            oneLifeTaken = false;
        }
    }

    public void Die()
    {
        flashAnimationManager.FlashDeathAnimation();
        alreadyDied = true;
        boxCollider.enabled = false;
        Time.timeScale = 0.3f;
        GameManager.Instance.ableToInput = false;
        playerAnimator.SetBool("IsBeingHit", false);
        armsAnimator.SetBool("IsBeingHit", false);
        playerAnimator.SetBool("IsDead", true);
    }

    public void CheckIfDied()
    {
        animatorState = playerAnimator.GetCurrentAnimatorStateInfo(0);

        if (animatorState.IsName("PlayerDead"))
        {
            gameOverManager.GameOver();
        }
    }

    private void InitializeBullets()
    {
        IList<string> weaponsStr = new List<string>(new string [] {"default", "metal", "thunder", "water", "fire"});
        for (int i = 0; i < weaponsStr.Count; i++)
        {
          weaponIcons.Add(weaponsStr[i], Resources.Load("Prefabs/" + weaponsStr[i] + "IconBlackWeapon"));
          selectedWeaponIcons.Add(weaponsStr[i], Resources.Load("Prefabs/" + weaponsStr[i] + "IconYellowWeapon"));
        }

        GameObject variableForPrefab = (GameObject)Resources.Load("Prefabs/PlayerBullet", typeof(GameObject));
        prefabs.Add("default", variableForPrefab);   
        playerBulletScriptDefault = variableForPrefab.GetComponent<PlayerBulletScript>();
        
        variableForPrefab = (GameObject)Resources.Load("Prefabs/PlayerBulletMetal", typeof(GameObject));
        prefabs.Add("metal", variableForPrefab);
        playerBulletScriptMetal = variableForPrefab.GetComponent<PlayerBulletMetal>();
        
        variableForPrefab = (GameObject)Resources.Load("Prefabs/PlayerBulletThunder", typeof(GameObject));
        prefabs.Add("thunder", variableForPrefab);
        playerBulletScriptThunder = variableForPrefab.GetComponent<PlayerBulletThunder>();
        
        variableForPrefab = (GameObject)Resources.Load("Prefabs/PlayerBulletWater", typeof(GameObject));
        prefabs.Add("water", variableForPrefab);
        playerBulletScriptWater = variableForPrefab.GetComponent<PlayerBulletWater>();
        
        variableForPrefab = (GameObject)Resources.Load("Prefabs/PlayerBulletFire", typeof(GameObject));
        prefabs.Add("fire", variableForPrefab);
        playerBulletScriptFire = variableForPrefab.GetComponent<PlayerBulletFire>();
        
        bullet = (GameObject)prefabs["default"];
    }

    public void InitializeAvailableWeapons() {
      weaponsAvailable.Add("default");
      if (GameManager.Instance.unlockedMetalBullet) weaponsAvailable.Add("metal");
      if (GameManager.Instance.unlockedThunderBullet) weaponsAvailable.Add("thunder");
      if (GameManager.Instance.unlockedWaterBullet) weaponsAvailable.Add("water");
      if (GameManager.Instance.unlockedFireBullet) weaponsAvailable.Add("fire");
    }

    private float ProcessShoot(string selectedShot, GameObject bullet, Vector3 playerPosition, Vector3 worldMousePosition)
    {
        float intervalSpeed = 0.1f;
        switch (selectedShot)
        {
            case "default":
                playerBulletScriptDefault.OnShoot(bullet, playerPosition + Vector3.up / 25, worldMousePosition);
                intervalSpeed = playerBulletScriptDefault.GetIntervalSpeed();
                break;
            case "metal":
                playerBulletScriptMetal.OnShoot(bullet, playerPosition, worldMousePosition);
                intervalSpeed = playerBulletScriptMetal.GetIntervalSpeed();
                break;
            case "thunder":
                playerBulletScriptThunder.OnShoot(bullet, playerPosition, worldMousePosition);
                intervalSpeed = playerBulletScriptThunder.GetIntervalSpeed();
                break;
            case "water":
                playerBulletScriptWater.OnShoot(bullet, playerPosition, worldMousePosition);
                intervalSpeed = playerBulletScriptWater.GetIntervalSpeed();
                break;
            case "fire":
                playerBulletScriptFire.OnShoot(bullet, playerPosition, worldMousePosition);
                intervalSpeed = playerBulletScriptFire.GetIntervalSpeed();
                break;
            default:
                playerBulletScriptDefault.OnShoot(bullet, playerPosition, worldMousePosition);
                intervalSpeed = playerBulletScriptDefault.GetIntervalSpeed();
                break;
        }
        return intervalSpeed;
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 11 && canBeHit)
        { // Hit Layer
            print("Player colidiu com hit");
            HasBeenHit();
        }
    }

    private void LoadArms()
    {
        Sprite variableForImage = (Sprite)Resources.Load("DynamicAssets/alien_thing_walk_fire_gun", typeof(Sprite));
        armsSprites.Add("firegun", variableForImage);
        variableForImage = (Sprite)Resources.Load("DynamicAssets/alien_thing_walk_gun", typeof(Sprite));
        armsSprites.Add("defaultgun", variableForImage);
        variableForImage = (Sprite)Resources.Load("DynamicAssets/alien_thing_walk_saw_gun", typeof(Sprite));
        armsSprites.Add("metalgun", variableForImage);
        variableForImage = (Sprite)Resources.Load("DynamicAssets/alien_thing_walk_water_gun", typeof(Sprite));
        armsSprites.Add("watergun", variableForImage);
        variableForImage = (Sprite)Resources.Load("DynamicAssets/alien_thing_walk_thunder_gun", typeof(Sprite));
        armsSprites.Add("thundergun", variableForImage);
    }

    void DrawWeapons() {
      // Destroy
      for (int i = 0; i < screenWeapons.Count; i++)
      {
          Destroy(screenWeapons[i]);
      }
      screenWeapons = new List<Object>();

      // Draw
      for (int i = 0; i < weaponsAvailable.Count; i++)
      {
        Object weaponIcon = weaponsAvailable[i] == selectedShot ? (GameObject) selectedWeaponIcons[weaponsAvailable[i]] : (GameObject) weaponIcons[weaponsAvailable[i]]; 
        screenWeapons.Add(Instantiate(weaponIcon, new Vector3(1.8f + i * 0.29f, -1.78f, 0), Quaternion.identity));
      }
    }

    private void PlayShotSound(string selectedShot)
    {
        switch (selectedShot)
        {
            case "default":
                FindObjectOfType<AudioManager>().Play("defaultShot");
                break;
            case "metal":
                FindObjectOfType<AudioManager>().Play("metalShot");
                break;
            case "thunder":
                FindObjectOfType<AudioManager>().Play("thunderShot");
                break;
            case "water":
                FindObjectOfType<AudioManager>().Play("waterShot");
                break;
            case "fire":
                FindObjectOfType<AudioManager>().Play("fireShot");
                break;
            default:
                FindObjectOfType<AudioManager>().Play("defaultShot");
                break;
        }
    }
}

