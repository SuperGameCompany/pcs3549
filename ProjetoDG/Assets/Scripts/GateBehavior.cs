﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GateBehavior : MonoBehaviour {

    public bool shouldSpawn = false;
    public Object enemyType;
    public Vector3 bossSpawnLocation;
    int currentLevelIndex;
    public bool waitingForSpawn = false;

    public void Start() {
        bossSpawnLocation = new Vector3(0f, 1.4f, 0f);
    }

    public void SpawnEnemy() {
        currentLevelIndex = SceneManager.GetActiveScene().buildIndex;

        // boss rush do level do exodia
        if (currentLevelIndex == 5) {
            if(GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 5) {
                Instantiate(Resources.Load("Prefabs/Boss1"), bossSpawnLocation, Quaternion.identity);
                GameManager.Instance.minionsSpawned++;
                return;
            }
            else if (GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 4) {
                if (GameManager.Instance.canSpawnBoss2) {
                    Instantiate(Resources.Load("Prefabs/Boss2"), bossSpawnLocation, Quaternion.identity);
                    GameManager.Instance.minionsSpawned++;
                    return;
                }
                else return;
            }
            else if (GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 3) {
                if (GameManager.Instance.canSpawnBoss3) {
                    Instantiate(Resources.Load("Prefabs/Boss3"), bossSpawnLocation, Quaternion.identity);
                    GameManager.Instance.minionsSpawned++;
                    return;
                }
                else return;
            }
            else if (GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 2) {
                if (GameManager.Instance.canSpawnBoss4) {
                    Instantiate(Resources.Load("Prefabs/Boss4"), bossSpawnLocation, Quaternion.identity);
                    GameManager.Instance.minionsSpawned++;
                    return;
                }
                else return;
            }
            else if (GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 1) {
                if (GameManager.Instance.canSpawnBoss5) {
                    Instantiate(Resources.Load("Prefabs/Boss5"), bossSpawnLocation, Quaternion.identity);
                    GameManager.Instance.minionsSpawned++;
                    return;
                }
                else return;
            }

        }

        if (GameManager.Instance.minionsSpawned == GameManager.Instance.maxEnemies - 1) {
            switch (currentLevelIndex) {
                case 1:
                    Instantiate(Resources.Load("Prefabs/Boss1"), bossSpawnLocation, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(Resources.Load("Prefabs/Boss2"), bossSpawnLocation, Quaternion.identity);
                    break;
                case 3:
                    Instantiate(Resources.Load("Prefabs/Boss3"), bossSpawnLocation, Quaternion.identity);
                    break;
                case 4:
                    Instantiate(Resources.Load("Prefabs/Boss4"), bossSpawnLocation, Quaternion.identity);
                    break;
                default:
                    break;
            }
            GameManager.Instance.minionsSpawned++;
        }
        else if (GameManager.Instance.minionsSpawned < GameManager.Instance.maxEnemies) {
            Instantiate(enemyType, transform.position, transform.rotation);
            GameManager.Instance.minionsSpawned++;
        }

    }
}
