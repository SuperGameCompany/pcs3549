﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterObstacleScript : MonoBehaviour {

    public float blizzardWarningTime = 0.7f;
    public float blizzardDuration = 3f;
    public float blizzardLaunchDelay = 2f;
    private bool canLaunchBlizzard = true;
    GameObject blizzard;
    SpriteRenderer selfSprite;

    // Use this for initialization
    void Start() {
        blizzard = GameObject.Find("Blizzard");
        blizzard.SetActive(false);
        selfSprite = GetComponent<SpriteRenderer>();
        selfSprite.enabled = false;
    }

    // Update is called once per frame
    void Update() {
        if (canLaunchBlizzard) {
            StartCoroutine(LaunchBlizzard());
        }
    }

    IEnumerator LaunchBlizzard() {
        canLaunchBlizzard = false;
        float randX = Random.Range(-2.5f, 2.4f);
        float randY = Random.Range(-0.75f, 0.88f);
        Vector3 newPosition = new Vector3(randX, randY, 1);
        //this.transform.SetPositionAndRotation()

        this.transform.position = newPosition;
        selfSprite.enabled = true;
        yield return new WaitForSeconds(blizzardWarningTime);
        blizzard.SetActive(true);
        selfSprite.enabled = false;
        yield return new WaitForSeconds(blizzardDuration);
        blizzard.SetActive(false);
        yield return new WaitForSeconds(blizzardLaunchDelay);
        canLaunchBlizzard = true;
    }

}
