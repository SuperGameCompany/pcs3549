﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletFire : MonoBehaviour {
    public int aoeDamage = 1;
    public float aoeRange = 1f;
    public float shootSpeed = 2.0f;
    public float shootInterval = 1.0f;
    public GameObject explosion;
    //public GameObject bullet;
    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    // OnTriggerEnter2D is called when the Collider2D other enters the trigger (2D physics only)
    public void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag != "Player")
        {
            Destroy(Instantiate(explosion, this.transform.position, Quaternion.identity), Time.deltaTime * 30);
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(this.transform.position, aoeRange);
            foreach (Collider2D collider in hitColliders)
            {
                if (collider.gameObject.tag == "Enemy") {
                    collider.GetComponent<EnemyMovement>().TakeDamage(aoeDamage);
                } else if (collider.gameObject.tag == "EnemyBullet") {
                    Destroy(collider.gameObject);
                }
            }
            Destroy(this.gameObject);
        }
    }

    public void OnShoot(GameObject bullet, Vector3 playerPosition, Vector3 worldMousePosition) {
        Vector2 shootDirection = worldMousePosition - playerPosition;
        shootDirection.Normalize();
        //this gives the angle between the x axis and the shooting vector
        float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        GameObject newBullet = Instantiate(bullet, playerPosition + (Vector3)(shootDirection * 0.2f), rotation);
        newBullet.GetComponent<Rigidbody2D>().velocity = shootDirection * shootSpeed;
    }

    public float GetIntervalSpeed() {
        return shootInterval;
    }
}
