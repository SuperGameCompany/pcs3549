﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss5 : Boss {
    public bool canChangeBullet = true;

    public override void Update() {
        base.Update();

        if (canChangeBullet) {
            StartCoroutine(ChangeBulletAndWait());
        }
    }

    public void ChangeBullet() {
        RangeWeapon weapon = this.gameObject.GetComponent<RangeWeapon>();
        GameObject[] bulletPrefabs = new GameObject[4];

        GameObject bossShurikenBullet = (GameObject)Resources.Load("prefabs/BossShurikenBullet", typeof(GameObject));
        bulletPrefabs[0] = bossShurikenBullet;

        GameObject boss2Bullet = (GameObject)Resources.Load("prefabs/Boss2Bullet", typeof(GameObject));
        bulletPrefabs[1] = boss2Bullet;

        GameObject boss3Bullet = (GameObject)Resources.Load("prefabs/Boss3Bullet", typeof(GameObject));
        bulletPrefabs[2] = boss3Bullet;

        GameObject boss4Bullet = (GameObject)Resources.Load("prefabs/Boss4Bullet", typeof(GameObject));
        bulletPrefabs[3] = boss4Bullet;

        int randomBulletIndex = Random.Range(0, 4);
        weapon.enemyBulletPrefab = bulletPrefabs[randomBulletIndex];
    }

    IEnumerator ChangeBulletAndWait() {
        canChangeBullet = false;
        ChangeBullet();

        float delay = Random.Range(0f,2.5f);
        yield return new WaitForSeconds(delay);

        canChangeBullet = true;
    }
}
