﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    private static GameManager instance;

    public int deadEnemies;
    public int minionsSpawned = 0;
    public bool gameIsPaused = false;
    public bool ableToInput = true;
    public bool gameOver = false;
    public int maxEnemies = 60;
    public int firstLevelMaxEnemies = 60;
    public int moreEnemiesEachLevel = 10;

    public bool unlockedMetalBullet = false;
    public bool unlockedThunderBullet = false;
    public bool unlockedWaterBullet = false;
    public bool unlockedFireBullet = false;

    public bool canSpawnBoss2 = false;
    public bool canSpawnBoss3 = false;
    public bool canSpawnBoss4 = false;
    public bool canSpawnBoss5 = false;

    public static GameManager Instance { get { return instance; } }

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    public void ResetInitialVariables() {
        deadEnemies = 0;
        minionsSpawned = 0;
        ableToInput = true;
        gameIsPaused = false;
        Time.timeScale = 1f;
        canSpawnBoss2 = false;
        canSpawnBoss3 = false;
        canSpawnBoss4 = false;
        canSpawnBoss5 = false;
}
}
