﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletMetal : PlayerBulletScript {
    public float rotateSpeed = 5f;
    public float radius = 0.1f;

    public PlayerBulletMetal() : base()
    {
        damage = 3;
        shootSpeed = 2.0f;
        shootInterval = 0.4f;

    }

    public new void OnShoot(GameObject bullet, Vector3 playerPosition, Vector3 worldMousePosition)
    {
        /*
        Vector2 centre = playerPosition;
        Vector2 shootDirection = worldMousePosition - playerPosition;
        float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;

        for (int i = 0; i < 100000; i++)
        {
            angle += rotateSpeed * Time.deltaTime;
            var offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * radius;
            transform.position = centre + offset;
        }
        */
        
        Vector2 shootDirection = worldMousePosition - playerPosition;
        shootDirection.Normalize();
        //this gives the angle between the x axis and the shooting vector
        float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        GameObject newBullet = Instantiate(bullet, playerPosition + (Vector3)(shootDirection * 0.2f), rotation);
        newBullet.GetComponent<Rigidbody2D>().velocity = shootDirection * shootSpeed;
        
    }
}
