﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleguidedBullet : EnemyBullet {

  Rigidbody2D body;
  public static Vector2 shootDirection;
  private GameObject player;
  private bool hasChangedDirection = false;
  private float velocity = 0.8f;
  private float timeToIncrease = 0;
  private float updateRate = 0.001f;
	// Use this for initialization
	void Start () {
		body = this.GetComponent<Rigidbody2D>();
    player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
    StartCoroutine(UpdateDirection());
	}

  IEnumerator UpdateDirection () {
    if (!hasChangedDirection) {
      hasChangedDirection = true;
      Vector3 selfPosition = transform.position;
      Vector3 playerPosition = player.transform.position;
      shootDirection = playerPosition - selfPosition;
      shootDirection.Normalize();
      float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;
      Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
      body.transform.position =  transform.position + (Vector3)(shootDirection * 0.001f);
      body.transform.rotation = rotation;
      body.velocity = shootDirection * velocity;
      yield return new WaitForSeconds(updateRate);
      timeToIncrease += 0.02f;
      if (timeToIncrease > 0.5f) {
        timeToIncrease = 0;
        velocity += 0.2f;
      }
      hasChangedDirection = false;
    }
  }
}
