using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    public enum EnemyTypes { Melee, Ranged };
    public EnemyTypes enemyType;
    public GameObject player;
    public float meleeAttackRange;
    public float meleeMoveSpeed;
    public float rangedAttackRange;
    public float rangedMoveSpeed;
    public GameObject rangedWeapon;
    public int enemyHP = 2;
    public float slowDuration = 2f;
    public bool reverseFlip = false;

    protected float moveSpeed;
    protected float attackRange;
    protected Rigidbody2D rigidBody;
    protected BoxCollider2D boxCollider;

    protected SpriteRenderer spriteRenderer;
    public Animator enemyAnimator;
    AnimatorStateInfo animatorState;
    bool dead = false;
    bool slowed = false;

    public bool isInAttackingPosition = false;

    // Use this for initialization
    public virtual void Start() {
        rigidBody = GetComponent<Rigidbody2D>();
        boxCollider = GetComponent<BoxCollider2D>();

        switch (enemyType) {
            case EnemyTypes.Melee:
                attackRange = meleeAttackRange;
                moveSpeed = meleeMoveSpeed;
                break;
            case EnemyTypes.Ranged:
                attackRange = rangedAttackRange;
                moveSpeed = rangedMoveSpeed;
                break;
        }

        player = GameObject.Find("Player");
    }

    // This function is called just one time by Unity the moment the game loads
    public virtual void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = false;
    }

    // Update is called once per frame
    public virtual void Update() {
        if (!dead) {
            MoveOrAttack();
        }
        else {
            IsDead();
        }

        //Flip sprite if necessary
        if (!reverseFlip) {
            if (!spriteRenderer.flipX && player.transform.position.x > this.transform.position.x) {
                spriteRenderer.flipX = true;
            }
            else if (spriteRenderer.flipX && player.transform.position.x < this.transform.position.x) {
                spriteRenderer.flipX = false;
            }
        }
        else {
            if (spriteRenderer.flipX && player.transform.position.x > this.transform.position.x) {
                spriteRenderer.flipX = false;
            }
            else if (!spriteRenderer.flipX && player.transform.position.x < this.transform.position.x) {
                spriteRenderer.flipX = true;
            }
        }
    }

    public virtual void Die() {
        dead = true;
        GameObject enemyTextController = GameObject.Find("TextController");
        EnemyCountTextController textControllerScript = enemyTextController.GetComponent<EnemyCountTextController>();
        textControllerScript.IncrementDeadEnemies();

        Destroy(boxCollider);
        Destroy(rigidBody);
        enemyAnimator.SetBool("IsDead", true);
    }

    public void IsDead() {
        animatorState = enemyAnimator.GetCurrentAnimatorStateInfo(0);

        if (animatorState.IsName("Dead")) {
            Destroy(gameObject);
        }
    }

    public void MoveOrAttack() {
        float speed = moveSpeed;
        if (slowed) {
            speed = moveSpeed / 2;
        }
        if (Vector3.Distance(player.transform.position, transform.position) <= attackRange ) {
            isInAttackingPosition = true;
            if(enemyAnimator != null) {
                enemyAnimator.SetBool("IsAttacking", true);
            }
        }
        else {
            rigidBody.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
            if(enemyAnimator != null) {
                enemyAnimator.SetBool("IsAttacking", false);
            }
            isInAttackingPosition = false;
        }
    }

    //public void OnTriggerEnter2D(Collider2D other) {
    //    if (other.gameObject.layer == 11) {
    //        print("Player acertado");
    //        PlayerScript player = other.GetComponent<PlayerScript>();
    //        player.HasBeenHit();
    //    }
    //}

    public void TakeDamage(int damage)
    {
        enemyHP -= damage;
        if (enemyHP <= 0 && !dead)
        {
            Die();
        }
    }

    IEnumerator Slow() {
        slowed = true;
        yield return new WaitForSeconds(slowDuration);
        slowed = false;
    }
}
