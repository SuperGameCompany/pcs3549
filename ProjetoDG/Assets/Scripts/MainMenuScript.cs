﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour {

    public void Start() {
        GameManager.Instance.ResetInitialVariables();
        GameManager.Instance.maxEnemies = GameManager.Instance.firstLevelMaxEnemies;
        GameManager.Instance.unlockedMetalBullet = false;
        GameManager.Instance.unlockedThunderBullet = false;
        GameManager.Instance.unlockedWaterBullet = false;
        GameManager.Instance.unlockedFireBullet = false;
        FindObjectOfType<AudioManager>().Play("bgm");
    }

    public void PlayButton() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitButton() {
        Application.Quit();
    }
}
