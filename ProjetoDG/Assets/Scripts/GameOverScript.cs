﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour {
    public GameObject gameOverMenu;
    int currentLevelIndex;

    public void Retry() {
        currentLevelIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentLevelIndex);
    }

    public void NextLevel() {
        currentLevelIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentLevelIndex == 1) {
            GameManager.Instance.unlockedMetalBullet = true;
        }
        else if (currentLevelIndex == 2) {
            GameManager.Instance.unlockedThunderBullet = true;
        }
        else if (currentLevelIndex == 3) {
            GameManager.Instance.unlockedWaterBullet = true;
        }
        else if (currentLevelIndex == 4) {
            GameManager.Instance.unlockedFireBullet = true;
        }
        SceneManager.LoadScene(currentLevelIndex + 1);
        GameManager.Instance.maxEnemies += GameManager.Instance.moreEnemiesEachLevel;
    }
}
