﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricObstacleScript : MonoBehaviour {

    public float thunderLaunchDelay = 3f;
    GameObject thunder;
    public Animator electricAnimator;
    AnimatorStateInfo animatorState;
    GameObject electricRayPrefab;
    GameObject rayWest;
    GameObject rayEast;
    GameObject rayNorth;
    GameObject raySouth;
    public float raySpeed = 150f;


    // Use this for initialization
    void Start() {
        thunder = GameObject.Find("Thunder");
        thunder.SetActive(false);
        electricRayPrefab = (GameObject)Resources.Load("prefabs/ElectricRay", typeof(GameObject));
    }

    // Update is called once per frame
    void Update() {
        animatorState = electricAnimator.GetCurrentAnimatorStateInfo(0);
        //Vector3 oldPosition = this.transform.position;

        if (animatorState.IsName("EndedBeginAnimation")) {
            electricAnimator.Play("ElectricThunderAnimation");
            thunder.SetActive(true);
            InstantiateElectricRays(this.transform.position);
        }

        if (animatorState.IsName("EndedThunderAnimation")) {
            thunder.SetActive(false);
            electricAnimator.Play("WaitingNextThunder");
            StartCoroutine(NextThunder());
        }
    }

    IEnumerator NextThunder() {
        float randX = Random.Range(-2.5f, 2.4f);
        float randY = Random.Range(-0.75f, 0.88f);
        Vector3 newPosition = new Vector3(randX, randY, 1);

        this.transform.position = newPosition;
        yield return new WaitForSeconds(thunderLaunchDelay);
        electricAnimator.Play("ElectricBeginAnimation");
    }

    private void InstantiateElectricRays(Vector3 thunderPosition) {
        Quaternion westRotation = Quaternion.AngleAxis(180, Vector3.forward);
        Quaternion eastRotation = Quaternion.AngleAxis(0, Vector3.forward);
        Quaternion northRotation = Quaternion.AngleAxis(90, Vector3.forward);
        Quaternion southRotation = Quaternion.AngleAxis(270, Vector3.forward);

        rayWest = Instantiate(electricRayPrefab, thunderPosition, westRotation);
        rayEast = Instantiate(electricRayPrefab, thunderPosition, eastRotation);
        rayNorth = Instantiate(electricRayPrefab, thunderPosition,northRotation);
        raySouth = Instantiate(electricRayPrefab, thunderPosition, southRotation);

        float raySpeedNormalized = raySpeed * Time.deltaTime;

        rayWest.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-raySpeedNormalized, 0);
        rayEast.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(raySpeedNormalized, 0);
        rayNorth.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, raySpeedNormalized);
        raySouth.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -raySpeedNormalized);
    }

}
