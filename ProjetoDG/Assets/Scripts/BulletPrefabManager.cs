﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPrefabManager : MonoBehaviour {

    public IList<GameObject> prefabs = new List<GameObject>();

    // Use this for initialization
    void Start () {
        GameObject variableForPrefab = (GameObject)Resources.Load("prefabs/PlayerBulletScript", typeof(GameObject));
        prefabs.Add(variableForPrefab);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject GetPrefab(int indice)
    {
        return prefabs[indice];
    }
}
