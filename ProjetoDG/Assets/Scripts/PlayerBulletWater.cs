﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletWater : MonoBehaviour {
    public int damage = 1;
    public float shootSpeed = 2.0f;
    public float shootInterval = 0.3f;
    public float range = 30f;
    //public GameObject bullet;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // OnTriggerEnter2D is called when the Collider2D other enters the trigger (2D physics only)
    public void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Obstacle") {
            Destroy(this.gameObject);
        }

        if (other.gameObject.tag == "Enemy") {
            GameObject enemyObject = other.gameObject;
            EnemyMovement enemyScript = enemyObject.GetComponent<EnemyMovement>();

            enemyScript.TakeDamage(damage);
            enemyScript.StartCoroutine("Slow");
            Destroy(this.gameObject);
        }
    }

    public void OnShoot(GameObject bullet, Vector3 playerPosition, Vector3 worldMousePosition)
    {
        Vector2 shootDirection = worldMousePosition - playerPosition;
        shootDirection.Normalize();
        //this gives the angle between the x axis and the shooting vector
        float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;

        for(int i = -2; i < 3; i++) {
            Vector2 newDirection = RotateVector(shootDirection, i * 20);
            Quaternion rotation = Quaternion.AngleAxis(angle + i * 20, Vector3.forward);
            GameObject newBullet = Instantiate(bullet, playerPosition + (Vector3)(shootDirection * 0.2f), rotation);
            newBullet.GetComponent<Rigidbody2D>().velocity = newDirection * shootSpeed;
            Destroy(newBullet, Time.deltaTime * range);
        }

    }

    public float GetIntervalSpeed()
    {
        return shootInterval;
    }

    private Vector2 RotateVector(Vector2 v, float angle)
    {
        float radian = angle * Mathf.Deg2Rad;
        float _x = v.x * Mathf.Cos(radian) - v.y * Mathf.Sin(radian);
        float _y = v.x * Mathf.Sin(radian) + v.y * Mathf.Cos(radian);
        return new Vector2(_x, _y);
    }
}
