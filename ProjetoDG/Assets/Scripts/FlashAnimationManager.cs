﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashAnimationManager : MonoBehaviour {
    public Animator flashAnimator;

	// Use this for initialization
	void Start () {
        flashAnimator = GetComponent<Animator>();
    }

    public void FlashDeathAnimation() {
        flashAnimator.Play("FlashDeathAnimation");
    }

    public void FlashHitAnimation() {
        flashAnimator.Play("FlashHitAnimation");
    }
}
