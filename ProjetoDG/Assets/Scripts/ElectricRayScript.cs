﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricRayScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision) {
        GameObject objectCollided = collision.gameObject;
        if (objectCollided.tag == "Wall") {
            Destroy(this.gameObject);
        }
    }


}
