﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalObstacleScript : MonoBehaviour {
    public float rotationSpeed = 2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0f, 0f, rotationSpeed * Time.deltaTime);
	}

}
