﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

    // Use this for initialization
    void Start() {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D hitInfo) {
        GameObject objectCollided = hitInfo.gameObject;

        if (objectCollided.tag == "Wall" || objectCollided.tag == "Obstacle") {
            Destroy(this.gameObject);
        }

        if (objectCollided.tag == "Player") {
            PlayerScript player = hitInfo.GetComponent<PlayerScript>();
            if (player.canBeHit) {
                Destroy(this.gameObject);
                player.HasBeenHit();
            }
        }

        if (objectCollided.tag == "PlayerBullet") {
            print("Bala do player acertada");
            Destroy(this.gameObject);
            Destroy(hitInfo.gameObject);
        }
    }
}
