﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireObstacleScript : MonoBehaviour {

    public float flameDuration = 2f;
    public float flameLaunchDelay = 2f;
    private bool canLaunchFlames = true;
    GameObject flames;

    // Use this for initialization
    void Start () {
        flames = GameObject.Find("Flames");
        flames.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (canLaunchFlames) {
            StartCoroutine(LaunchFlames());
        }
	}

    IEnumerator LaunchFlames() {
        canLaunchFlames = false;
        flames.SetActive(true);
        yield return new WaitForSeconds(flameDuration);
        flames.SetActive(false);
        yield return new WaitForSeconds(flameLaunchDelay);
        canLaunchFlames = true;
    }

}
