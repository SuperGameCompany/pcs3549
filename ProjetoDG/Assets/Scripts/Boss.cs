﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : EnemyMovement {
    //public GameObject gateObject;
    //public GateBehavior gateScript;


    public override void Awake() {
        base.Awake();
        //gateObject = GameObject.Find("GateD");
        //gateScript = gateObject.GetComponent<GateBehavior>();
    }

    public override void Die() {
        if (gameObject.name.Equals("Boss1(Clone)")) {
            GameManager.Instance.canSpawnBoss2 = true;
        }
        else if(gameObject.name.Equals("Boss2(Clone)")) {
            GameManager.Instance.canSpawnBoss3 = true;
        }
        else if (gameObject.name.Equals("Boss3(Clone)")) {
            GameManager.Instance.canSpawnBoss4 = true;
        }
        else if (gameObject.name.Equals("Boss4(Clone)")) {
            GameManager.Instance.canSpawnBoss5 = true;
        }

        base.Die();
    }

    public void DestroyObject() {
        Destroy(gameObject);
    }
}
