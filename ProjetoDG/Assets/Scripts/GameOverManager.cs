﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour {
    public GameObject gameOverMenu;
    public GameObject playerObject;
    public PlayerScript player;

    public GameObject nextButton;
    public TextMeshProUGUI nextButtonText;
    public GameObject retryButton;

    public GameObject gameOverTextObject;
    public TextMeshProUGUI gameOverText;

    public GameObject winOrLoseTextMeshObject;
    public TextMeshProUGUI winOrLoseTextMesh;

    public GameObject unlockedWeaponTextObject;
    public TextMeshProUGUI unlockedWeaponText;

    public GameObject pressToSwitchTextObject;
    public TextMeshProUGUI pressToSwitchText;

    public GameObject finishedTheGameTextObject;
    public TextMeshProUGUI finishedTheGameText;

    public GameObject weaponIconObject;

    public void Start() {
        gameOverMenu = GameObject.Find("GameOverMenu");
        playerObject = GameObject.Find("Player");
        player = playerObject.GetComponent<PlayerScript>();


        nextButton = GameObject.Find("NextButton");
        retryButton = GameObject.Find("RetryButton");

        nextButtonText = GameObject.Find("NextText").GetComponent<TextMeshProUGUI>();

        gameOverTextObject = GameObject.Find("GameOverText");
        gameOverText = gameOverTextObject.GetComponent<TextMeshProUGUI>();

        winOrLoseTextMeshObject = GameObject.Find("YouWinOrLoseText");
        winOrLoseTextMesh = winOrLoseTextMeshObject.GetComponent<TextMeshProUGUI>();

        unlockedWeaponTextObject = GameObject.Find("UnlockedWeaponText");
        unlockedWeaponText = unlockedWeaponTextObject.GetComponent<TextMeshProUGUI>();

        pressToSwitchTextObject = GameObject.Find("PressToSwitchText");
        pressToSwitchText = pressToSwitchTextObject.GetComponent<TextMeshProUGUI>();

        finishedTheGameTextObject = GameObject.Find("FinishedTheGame");
        finishedTheGameText = finishedTheGameTextObject.GetComponent<TextMeshProUGUI>();

        weaponIconObject = GameObject.Find("WeaponIcon");

        nextButtonText.text = "Next Level";
        finishedTheGameTextObject.SetActive(false);

        //The x scale of the menu is 0 to make it disappear when editing the project
        //on the Unity interface.
        gameOverMenu.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        gameOverMenu.SetActive(false);

    }

    // Update is called once per frame
    void Update() {
        CheckIfWonOrLose();
    }

    public void GameOver() {
        Time.timeScale = 0f;
        GameManager.Instance.ableToInput = false;

        gameOverMenu.SetActive(true);
    }

    public void CheckIfWonOrLose() {
        //win
        if (GameManager.Instance.deadEnemies >= GameManager.Instance.maxEnemies) {
            int currentLevelIndex = SceneManager.GetActiveScene().buildIndex;

            gameOverText.gameObject.SetActive(false);
            winOrLoseTextMesh.text = "You Win!";
            nextButton.SetActive(true);
            retryButton.SetActive(false);
            unlockedWeaponTextObject.SetActive(true);
            pressToSwitchTextObject.SetActive(true);
            pressToSwitchText.text = "Press " + (currentLevelIndex + 1) + " to switch: ";

            if (currentLevelIndex == 1) {
                weaponIconObject.GetComponent<Image>().sprite = (Sprite)Resources.Load("weaponsIcons/metalWeapon", typeof(Sprite));
            }
            else if (currentLevelIndex == 2) {
                weaponIconObject.GetComponent<Image>().sprite = (Sprite)Resources.Load("weaponsIcons/electricWeapon", typeof(Sprite));
            }
            else if (currentLevelIndex == 3) {
                weaponIconObject.GetComponent<Image>().sprite = (Sprite)Resources.Load("weaponsIcons/waterWeapon", typeof(Sprite));
            }
            else if (currentLevelIndex == 4) {
                weaponIconObject.GetComponent<Image>().sprite = (Sprite)Resources.Load("weaponsIcons/fireWeapon", typeof(Sprite));
            }
            else if (currentLevelIndex == 5) {
                unlockedWeaponTextObject.SetActive(false);
                pressToSwitchTextObject.SetActive(false);
                weaponIconObject.SetActive(false);
                finishedTheGameTextObject.SetActive(true);
                nextButtonText.text = "Credits";
            }

            StartCoroutine(WaitForWin());
        }

        //lose
        if (!player.alreadyDied && player.lifes.Count <= 0) {
            gameOverText.gameObject.SetActive(true);
            winOrLoseTextMesh.text = "You Lose!";
            nextButton.SetActive(false);
            retryButton.SetActive(true);
            unlockedWeaponTextObject.SetActive(false);
            pressToSwitchTextObject.SetActive(false);
            weaponIconObject.SetActive(false);
            player.Die();
        }
    }

    IEnumerator WaitForWin() {
        player.playerAnimator.SetBool("IsBeingHit", true);
        player.armsAnimator.SetBool("IsBeingHit", true);
        player.oneLifeTaken = true;

        yield return new WaitForSeconds(1.5f);
        GameOver();
    }
}
