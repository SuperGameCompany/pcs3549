﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour {

    private GameObject[] gates;
    public float spawnTime = 5f; //Time between each spawn
    public bool spawnMelee = true;
    public bool spawnRanged = true;
    public float maxMinions;

    private GateBehavior selectedGateBehavior;
    private Object enemyType;

    Object enemyMelee;
    Object enemyRanged;

    // Use this for initialization
    void Start() {
        gates = GameObject.FindGameObjectsWithTag("Gate");
        enemyMelee = Resources.Load("Prefabs/enemyMelee");
        enemyRanged = Resources.Load("Prefabs/enemyRanged");
        maxMinions = GameManager.Instance.maxEnemies;


        StartCoroutine(SpawnLoop());
    }

    IEnumerator SpawnLoop() {
        while (GameManager.Instance.minionsSpawned < maxMinions) {
            float prob = 0.5f;
            if(spawnMelee && spawnRanged) {
                prob = 0.5f;
            }
            else if(spawnMelee){
                prob = 0f;
            }
            else if (spawnRanged) {
                prob = 1f;
            }
            else {
                prob = -10f;
            }


            if (Random.value > prob) {
                enemyType = enemyMelee;
            }
            else {
                enemyType = enemyRanged;
            }

            if(prob != -10f) {
                PickGateForSpawn();
            }

            yield return new WaitForSeconds(spawnTime);
        }
    }

    private void PickGateForSpawn() {
        GameObject gate = gates[Random.Range(0, gates.Length)];
        selectedGateBehavior = gate.GetComponent<GateBehavior>();
        selectedGateBehavior.enemyType = enemyType;
        Animator animator = gate.GetComponent<Animator>();
        animator.SetTrigger("AnimateDoor");
    }
}
