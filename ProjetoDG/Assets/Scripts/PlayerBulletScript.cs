﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletScript : MonoBehaviour {
    public int damage = 1;
    public float shootSpeed = 5.0f;
    public float shootInterval = 0.1f;
    //public GameObject bullet;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // OnTriggerEnter2D is called when the Collider2D other enters the trigger (2D physics only)
    public void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Obstacle") {
            Destroy(this.gameObject);
        }

        if (other.gameObject.tag == "Enemy") {
            GameObject enemyObject = other.gameObject;
            EnemyMovement enemyScript = enemyObject.GetComponent<EnemyMovement>();

            enemyScript.TakeDamage(damage);
            Destroy(this.gameObject);
        }
    }

    public void OnShoot(GameObject bullet, Vector3 playerPosition, Vector3 worldMousePosition)
    {
        Vector2 shootDirection = worldMousePosition - playerPosition;
        shootDirection.Normalize();
        //this gives the angle between the x axis and the shooting vector
        float angle = Mathf.Atan2(shootDirection.y, shootDirection.x) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        GameObject newBullet = Instantiate(bullet, playerPosition + (Vector3)(shootDirection * 0.2f), rotation);
        newBullet.GetComponent<Rigidbody2D>().velocity = shootDirection * shootSpeed;
    }

    public float GetIntervalSpeed()
    {
        return shootInterval;
    }
}
