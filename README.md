## PCS3549 - Design e Programação de Games (2018)

Jogo estilo RPG de ação em que o jogador controla um robô gladiador em batalhas de arena, construído puramente para entretenimento humano.

### Observações
Este projeto está utilizando a versão 2018.2.7, que aparentemente é o último build exato que existe simultaneamente para Windows/Mac/Linux.

* [Windows 64 bits](https://unity3d.com/pt/get-unity/download?thank-you=update&download_nid=58241&os=Win)
* [Linux](https://beta.unity3d.com/download/dad990bf2728/public_download.html)
* [MacOS](https://unity3d.com/pt/get-unity/download?thank-you=update&download_nid=58241&os=Mac)
