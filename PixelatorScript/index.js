const { exec } = require('child_process');

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

async function humbleExec (humbleCmd, prefix) {
	return new Promise((resolve, reject) => {
		try {
			exec(humbleCmd, () => {
				console.log(`Terminei!! ${prefix}`)
				resolve()
			})
		} catch (err) {
			reject(err)
		}
	})
}
async function pixelateManyImages (path, lastIndex, prefix, extension = `.png`) {
	console.log(`Pixelando com prefix ${prefix}, de 0 a ${lastIndex}`)
	for (let i = 0; i <= lastIndex; i++) {
		const humble =  `"./_pixelator_cmd.exe"` + ` "${path}\\${prefix}${pad(i, 3)}${extension}" "${path}\/${prefix}${pad(i, 3)}_pixel${extension}"` + ` --pixelate 5 --colors 64 --palette_mode adaptive --enhance 2 --smooth 2 --smooth_iterations 1 --refine_edges 250 --stroke outside --stroke_opacity 1 --stroke_on_colors_diff 0 --background "#00000000"    --palette_file "" --override`
		await humbleExec(humble, prefix)
	}
}


const root = `D:\\Assets\\HumbleBundle\\2dgameartbundle\\Dumb\ Robot\\PNG\\PNG\ Sequenes\\`

const listOfSubFolders = [
	{
		folder: `Attacking`,
		lastIndex: 11
	},
	{
		folder: `Dying`,
		lastIndex: 14
	},
	{
		folder: `Running`,
		lastIndex: 14
	},
	/*{
		folder: `Falling\ Down`,
		lastIndex: 5
	},
	{
		folder: `Hurt`,
		lastIndex: 11
	},
	{
		folder: `Idle`,
		lastIndex: 17
	},
	{
		folder: `Jump\ Loop`,
		lastIndex: 5
	},
	{
		folder: `Jump\ Start`,
		lastIndex: 5
	},
	{
		folder: `Sliding`,
		lastIndex: 5
	},
	{
		folder: `Walking`,
		lastIndex: 23
	}*/
]

const doWork = async () => {
	for (const subfolder of listOfSubFolders) {
		await pixelateManyImages(root + subfolder.folder, subfolder.lastIndex, subfolder.prefix ? subfolder.prefix : subfolder.folder + `_`);
	}
}

doWork()